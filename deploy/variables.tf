variable "prefix" {
  type        = string
  default     = "raad"
  description = "description"
}

variable "project" {
  type        = string
  default     = "recipe-app-api-devops"
  description = "description"
}

variable "contact" {
  type        = string
  default     = "email@gmail.com"
  description = "description"
}


